﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using Expression = org.mariuszgromada.math.mxparser.Expression;

namespace wpf_calc_beta
{
    /// <summary>
    /// Interaction logic for CalcBeta.xaml
    /// </summary>
    public partial class CalcBeta
    {
        #region Constants
        private const string Pi = "π";
        private const string Mult = "×";
        private const string Div = "÷";
        private const string Root = "√";
        private const string Deg = "°";
        #endregion
        public CalcBeta() // Инициализация калькулятора
        {
            InitializeComponent();
            MathTb.Text = "0";
            TextBoxes.RowDefinitions[1].Height = new GridLength(0.0, GridUnitType.Star); // Прячем результат
        }

        #region Calculation

        private void EvalExpression(string expr) //Вычисление
        {
            while (!IsNumber(expr) && !expr.EndsWith(")") && !expr.EndsWith("!") &&
                   !expr.EndsWith(Deg) && !expr.EndsWith(Pi) && !expr.EndsWith("e"))
            {
                if (expr == string.Empty)
                {
                    ResultTb.Text = "0";
                    return;
                }
                expr = expr.Remove(expr.Length - 1);
            }
            expr = PlaceBr(expr);
            var res = new Expression(ReplaceActions(expr)).calculate();
            if (!double.IsNaN(res))
            {
                ResultTb.Text = res.ToString(CultureInfo.CurrentCulture).Replace(",", ".");
                TextBoxes.RowDefinitions[1].Height = expr == ResultTb.Text ? new GridLength(0.0, GridUnitType.Star) : new GridLength(50.0, GridUnitType.Star);
            }
            else ResultTb.Text = ResultTb.Text;
        }

        #endregion

        #region OtherFunctions
        #region ReadWriteFuncs
        private void AddString(string s) => MathTb.Text += s; //Функция записи символа в пример
        private void RemoveString(int amt) => MathTb.Text = MathTb.Text.Remove(MathTb.Text.Length - amt); //Удаление n-символов из примера
        #endregion
        #region ActionFuncs
        private static bool CheckForAction(string s) =>
            s.EndsWith("+") || s.EndsWith("-") || s.EndsWith(Mult) || s.EndsWith(Div) || s.EndsWith("^"); //Проверка примера на последний знак действия
        private static string ReplaceActions(string s) => s.Replace(Mult, "*").Replace(Div, "/").Replace(Root, "sqrt")
            .Replace(Pi, "pi").Replace(Deg, "*[deg]").Replace("lg", "log10");
        private static int RmActCheck(string s)
        {
            if (s.EndsWith("sin(") || s.EndsWith("cos(") || s.EndsWith("tan(") || s.EndsWith("ctg(") || s.EndsWith("log(")) return 4;
            if (s.EndsWith("lg(") || s.EndsWith("ln(")) return 3;
            if (s.EndsWith("√(") || s.Length == 2 && s[0] == '-') return 2;
            return 1;
        }
        private static bool IsMultNeed(string s) => s.EndsWith(")") || s.EndsWith(Pi) || s.EndsWith("e") || s.EndsWith(Deg);
        private static bool IsNumber(string s)
        {
            if (s == string.Empty) return false;
            return int.TryParse(s[s.Length - 1].ToString(), out _); //Проверка, что последний символ в примере число
        }
        #endregion
        #region BracketFuncs
        private static int GetBr(string s) //Получение числа скобок
        {
            var oBr = 0; //Кол-во открывающихся скобок
            var cBr = 0; //Кол-во закрывающихся скобок
            foreach (var c in s) //Узнаем циклом сколько каких скобок
            {
                switch (c)
                {
                    case '(':
                        oBr++;
                        break;
                    case ')':
                        cBr++;
                        break;
                }
            }

            return oBr - cBr;
        }

        private static string PlaceBr(string s) //Добавление нехватающих скобок
        {
            var brAmt = GetBr(s);
            for (var i = 0; i < brAmt; i++) //Добавляем в пример отсутствующие скобки циклом
            {
                s += ')';
            }

            return s;
        }
        #endregion
        #endregion

        #region ActionClickFunctions

        #region SimpleActions
        private void NumClick(object sender, RoutedEventArgs e)
        {
            var s = ((Button)e.Source).Content.ToString();
            if (MathTb.Text == "0") RemoveString(1);
            if (IsMultNeed(MathTb.Text)) AddString(Mult); //Ставится умножение при определенных условиях
            AddString(s);
            EvalExpression(MathTb.Text);
        }
        private void ActClick(object sender, RoutedEventArgs e)
        {
            var s = ((Button)e.Source).Content.ToString();
            #region MinusAfterSymbols
            if ((MathTb.Text.EndsWith("(") || MathTb.Text.EndsWith("^") || CheckForAction(MathTb.Text) && !MathTb.Text.EndsWith("-") && !MathTb.Text.EndsWith("+")) && s == "-") 
            {
                AddString(s);
                return; //Тут короче код отвечающий за минус после открывающейся скобки
            }
            if ((MathTb.Text.EndsWith("(-") || MathTb.Text.EndsWith("^-") || MathTb.Text.EndsWith(Mult + "-") || MathTb.Text.EndsWith(Div + "-")) && s == "+") //Если нажали плюс, то минус после скобки убирается
            {
                RemoveString(1);
                return;
            }
            else if (MathTb.Text.EndsWith("(-") || MathTb.Text.EndsWith("^-") || MathTb.Text.EndsWith(Mult + "-") || MathTb.Text.EndsWith(Div + "-")) return; //на случай чтобы не получить вместо минуса любое другое действие
            #endregion

            switch (MathTb.Text)
            {
                case "0" when s == "-":
                    MathTb.Text = "-";
                    break;
                case "-" when s == "+":
                    MathTb.Text = "0";
                    return;
                case "-" when s == Mult || s == Div:
                    return;
            }
            if (MathTb.Text.EndsWith(",")) return;
            if (CheckForAction(MathTb.Text) || MathTb.Text.EndsWith(".")) RemoveString(1);
            AddString(s);
        }
        #endregion
        #region AdditionalActions
        private void DotClick(object sender, RoutedEventArgs e)
        {
            var s = ((Button)e.Source).Content.ToString();
            if (!IsNumber(MathTb.Text)) return;
            for (var i = MathTb.Text.Length - 1; i >= 0; i--) //Цикл нужен для того чтобы всякие гении не ставили в одном числе две точки
            {
                if (CheckForAction(MathTb.Text[i].ToString())) break;
                if (MathTb.Text[i].ToString() == s) return;
            }
            AddString(s);
        }
        private void EqualClick(object sender, RoutedEventArgs e)
        {
            MathTb.Text = ResultTb.Text; //Пока нажатие на равно заменяет пример на результат от решения
            TextBoxes.RowDefinitions[1].Height = new GridLength(0.0, GridUnitType.Star);
        }
        private void BracketClick(object sender, RoutedEventArgs e)
        {
            var s = ((Button)e.Source).Content.ToString();
            if (GetBr(MathTb.Text) <= 0 && s == ")") return;
            if ((CheckForAction(MathTb.Text) || MathTb.Text.EndsWith("(")) && s == ")") return; //Проверка чтобы не ставили закрывающую скобку после действия и открывающей скобки
            if (MathTb.Text.EndsWith(".") || MathTb.Text == "0") RemoveString(1); //Если перед скобкой точка, или весь пример это 0, то убираем точку/ноль
            if (MathTb.Text != string.Empty && s == "(" && (IsNumber(MathTb.Text) || MathTb.Text.EndsWith(")") || MathTb.Text.EndsWith(Pi) || MathTb.Text.EndsWith("e") || MathTb.Text.EndsWith(Deg))) AddString(Mult) ; //Если после числа нет действий то ставим умножение
            AddString(s);
        }
        #endregion
        #region AdvancedActions
        private void PowClick(object sender, RoutedEventArgs e)
        {
            const string s = "^";
            if (CheckForAction(MathTb.Text) || MathTb.Text.EndsWith("(") || MathTb.Text.EndsWith(s)) return;
            if (CheckForAction(MathTb.Text) || MathTb.Text.EndsWith(".")) RemoveString(1);
            AddString(s);
        }
        private void FactClick(object sender, RoutedEventArgs e)
        {
            var s = ((Button)e.Source).Content.ToString();
            if (IsNumber(MathTb.Text) || MathTb.Text.EndsWith(Pi) || MathTb.Text.EndsWith("e") || MathTb.Text.EndsWith(")"))
            {
                AddString(s);
                EvalExpression(MathTb.Text);
            }
        }
        private void SqrtClick(object sender, RoutedEventArgs e)
        {
            const string s = "√(";
            if (MathTb.Text.EndsWith(".") || MathTb.Text == "0") RemoveString(1);
            if (IsNumber(MathTb.Text) && MathTb.Text.Length != 0 || MathTb.Text.EndsWith(Pi) || MathTb.Text.EndsWith("e")) AddString(Mult); //Тут проверка чтобы знак умножения ставился сам в определенных случаях
            AddString(s);
        }
        #endregion
        #region ConstFuncMenus
        private void ConstClick(object sender, RoutedEventArgs e)
        {
            var s = ((Button)e.Source).Content.ToString();
            if (MathTb.Text == "0" || MathTb.Text.EndsWith(".")) RemoveString(1);
            if (IsNumber(MathTb.Text) && MathTb.Text.Length != 0 || IsMultNeed(MathTb.Text)) AddString(Mult);
            ConstBack(null, null);
            AddString(s);
            EvalExpression(MathTb.Text);
        }
        private void FuncClick(object sender, RoutedEventArgs e)
        {
            var s = ((Button)e.Source).Content.ToString();
            if (MathTb.Text == "0" || MathTb.Text.EndsWith(".")) RemoveString(1);
            if (IsNumber(MathTb.Text) && MathTb.Text.Length != 0 || IsMultNeed(MathTb.Text)) AddString(Mult);
            FuncBack(null, null);
            if (s == "log(x,y)") s = "log";
            AddString(s + "(");
            EvalExpression(MathTb.Text);
        }

        private void SpecClick(object sender, RoutedEventArgs e)
        {
            var s = ((Button) e.Source).Content.ToString();
            if (s == ",")
            {
                var logPart = string.Empty;
                for (var i = MathTb.Text.LastIndexOf("log", StringComparison.Ordinal); i < MathTb.Text.Length; i++)
                {
                    logPart += MathTb.Text[i];
                }

                var logBr = GetBr(logPart);
                for (var i = 0; i < logBr; i++)
                {
                    MathTb.Text += ")";
                }
                RemoveString(1);
            }
            if (IsNumber(MathTb.Text) || MathTb.Text.EndsWith(Pi) || MathTb.Text.EndsWith("e") || MathTb.Text.EndsWith(")")) AddString(s);
            FuncBack(null, null);
            EvalExpression(MathTb.Text);
        }
        #endregion
        #endregion

        #region ClickFunctions
        private void RemoveClick(object sender, RoutedEventArgs e) //Удаление последнего действия/числа
        {
            RemoveString(RmActCheck(MathTb.Text));
            if (MathTb.Text == string.Empty) MathTb.Text = "0";
            EvalExpression(MathTb.Text);
        }
        private void ClearClick(object sender, RoutedEventArgs e) //Сброс калькулятора
        {
            MathTb.Text = "0";
            ResultTb.Text = "0";
            TextBoxes.RowDefinitions[1].Height = new GridLength(0.0, GridUnitType.Star);
        }

        #endregion

        #region ShowHideFunctions
        private void ConstShow(object sender, RoutedEventArgs e)
        {
            ConstGrid.Visibility = Visibility.Visible;
            NumGrid.Visibility = Visibility.Hidden;
        }
        private void ConstBack(object sender, RoutedEventArgs e)
        {
            ConstGrid.Visibility = Visibility.Hidden;
            NumGrid.Visibility = Visibility.Visible;
        }
        private void FuncShow(object sender, RoutedEventArgs e)
        {
            FuncGrid.Visibility = Visibility.Visible;
            NumGrid.Visibility = Visibility.Hidden;
        }
        private void FuncBack(object sender, RoutedEventArgs e)
        {
            FuncGrid.Visibility = Visibility.Hidden;
            NumGrid.Visibility = Visibility.Visible;
        }
        #endregion
    }
}
